# Build API Manager Image

This project build API Manger image using "APIGateway_7.7.20220228-DockerScripts-2.4.0" from Gateway release 7.7 on 28th
Feb 2022. It produces Docker image for API Manger using 'apim_base:7_7_20220228'. So for this build to work, it needs '
apim_base:7_7_20220228' image avaialel in connected Docker registry.

This project supports build for all supported environment, as blew:

- dev
- sit
- uat
- prod

## Environment Specific Certificates

Certificates for supported environments should be placed in 'apimgr/certs' in environment specific folder. For example,
for 'SIT' environment the 'SIT' specific certificates should go under 'apimgr/certs/sit' folder, as below:

- The certificate with the name 'cert.pem'
- The private key with the 'key.pem'
- The passphrase file with the name 'pass.txt'

## Environment Specific Fed and acl.json files

The environment specific Fed and acl.json files should be placed in 'apimgr/confs' in environment specific folder. For
example for 'SIT' environment the 'SIT' specific Fed and acl.json files should go under 'apimgr/confs/sit' folder, as
below:

- fed folder containing all fed related files
- acl.json

## Environment Specific environment configuration file

The environment specific environment configuration file 'bct.env' should be placed in 'apimgr/environment' in
environment specific folder. For example for 'SIT' environment the 'SIT' specific 'bct.env' file should go under '
apimgr/environment/sit' folder, as below:

- apimgr/environment/dev/bct.env

## Environment Specific groups configuration files

The environment specific groups configuration files should be placed in 'apimgr/groups' in environment specific folder.
For example for 'SIT' environment the 'SIT' specific groups configuration files should go under 'apimgr/groups/sit'
folder, as below:

- apimgr/groups/sit/groups/emt-group/emt-service/conf
    - envSettings.props
    - jvm.xml

### Policy File

There is single policy file that remains same across all environment. The policy file with name 'bct.pol' goes under '
apimgr/policy' folder.

### Notes:

- This project differentiates between prod and non-prod builds. All non-prod builds go into 'apim/apimgr_snapshot'
  docker image repository whereas, all prod builds go into 'apim/apimgr_release' docker image repository.
- The jenkins build is defined in 'Jenkinsfile'. Below is the snippet of build pipeline stages:
- 
![Jenkins Build View](Jenkins Build Screenshot.png)

- The Harbor project, repositories and artifacts release look like below:

![Harbor Project](Harbor Project Screenshot.png)

![Harbor repositories](Harbor repositories Screenshot.png)

![Artifacts](Harbor apimgr_snapshot.png)

### Test Docker Image

The Docker image for API Manager can be tested by following below steps:

#### MySQL Database Server needs to be Available and Accessible from APIM Docker Image
#### Cassandra Database Server needs to be Available and Accessible from APIM Docker Image
#### Create a Docker Network with name for example:
- 'api-gateway-domain' using command 'docker network create api-gateway-domain'
#### Create logs folder for example:
- 'mkdir -p ~/merge-dir/apigateway/logs'
#### Create events folder for example:
- 'mkdir -p ~/merge-dir/apigateway/events'
#### Create licenses folder for example:
- 'mkdir -p ~/merge-dir/apigateway/conf/licenses'
#### Copy license to the previously created licenses folder for example:
-'cp lic20.lic conf/licenses/'

#### Run API Manager
##### Note: the image name 'apim_apig_202204:7.7' is just example in below 'docker run' command, use the image name produced by this build instead.
  docker run 
    -d 
    --name=apimgr77 
    --network=api-gateway-domain 
    -p 8075:8075 
    -p 8065:8065 
    -p 8080:8080 
    -v ~/merge-dir/apigateway/events:/opt/Axway/apigateway/events 
    -v ~/merge-dir/apigateway/logs:/opt/Axway/apigateway/logs 
    -v ~/merge-dir/apigateway/conf/licenses:/opt/Axway/apigateway/conf/licenses 
    -e EMT_ANM_HOSTS=anm77:8090 
    -e EMT_DEPLOYMENT_ENABLED=true 
    -e CASS_HOST1=172.16.63.177 
    -e CASS_HOST2=172.16.63.177 
    -e CASS_HOST3=172.16.63.177 
    -e CASS_PORT1=9042 
    -e CASS_PORT2=9042 
    -e CASS_PORT3=9042 
    -e CASS_USERNAME=cassandra 
    -e CASS_PASS=changeme 
    -e METRICS_DB_URL=jdbc:mysql://172.16.63.178:3306/metrics?useSSL=false 
    -e METRICS_DB_USERNAME=gateway_user 
    -e METRICS_DB_PASS=changeme 
    -e EMT_TRACE_LEVEL=INFO 
    -e ACCEPT_GENERAL_CONDITIONS=yes 
    apim_apig_202204:7.7

















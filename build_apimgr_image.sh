#! /bin/bash

echo
echo "----------- Building API Manager for environment '$1' Using Base Image '$3'----------- "
tar xf APIGateway_7.7.20220228-DockerScripts-2.4.0.tar

# get the certificate from environment variable
echo "${APIGATEWAY_LICENSE}" | base64 -d >license.lic
echo "----------- Using following license ----------- "
cat license.lic

echo "----------- coping conf for '$1' to merge-dir/apigateway/conf ----------- "
cp -r apimgr/confs/$1/conf apimgr/merge-dir/apigateway/

echo "----------- coping groups for '$1' to merge-dir/apigateway/groups ----------- "
cp -r apimgr/groups/$1/groups apimgr/merge-dir/apigateway/

./apigw-emt-scripts-2.4.0/build_gw_image.py \
  --license=license.lic \
  --domain-cert=apimgr/certs/$1/cert.pem \
  --domain-key=apimgr/certs/$1/key.pem \
  --domain-key-pass-file=apimgr/certs/$1/pass.txt \
  --merge-dir apimgr/merge-dir/apigateway \
  --pol=apimgr/policy/bct.pol \
  --env=apimgr/environment/$1/bct.env \
  --fed-pass-file=apimgr/nopass.txt \
  --parent-image=$4/$3 \
  --out-image=$4/apim/apimgr$5:$2
